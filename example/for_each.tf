provider "aws" {
    region = "us-east-1"
}



resource "aws_instance" "instance1" {
    for_each = toset(var.image_id)
    ami = each.value
    instance_type = "t2.micro"
    vpc_security_group_ids = ["sg-0dbb47aab64f30ec3"]
  
  #### FILE PROVISIONER ######
  provisioner "file" {
    source      = "index.html"
    destination = "/var/www/html/index.html"
  }
  
 ##### LOCAL PROVISIONER ######## 
    provisioner "local-exec" {
    command = "echo "TERRAFORM SUCCUSSFULLY EXECUTED" > /var/log/tf/tf.log"
  }

 #### REMOTE PROVISIONER #######
   connection {
    type     = "ssh"
    user     = "ec2-user"
    private_key = var.my-key
    host     = self.public_ip
  }

  provisioner "remote-exec" {
    inline = [
      "yum install httpd -y",
      "systemctl start httpd",
      "systemctl enable httpd"
    ]
  }


}

variable "image_id" {
    type = list(string)
    default = ["ami-079db87dc4c10ac91", "ami-00b8917ae86a424c9", "ami-0c7217cdde317cfec", "ami-023c11a32b0207432"]
}

output "ip" {
    value = [
        for instance in var.image_id:
        aws_instance.instance1[instance].public_ip
    ]    
}
