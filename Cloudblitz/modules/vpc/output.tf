output "ssh_sg_id" {
    value = aws_security_group.allow_ports["sg-ssh"].id
}

output "http_sg_id" {
    value = aws_security_group.allow_ports["sg-http"].id
}

output "tomcat_sg_id" {
    value = aws_security_group.allow_ports["sg-tomcat"].id
}

output "rds_sg_id" {
    value = aws_security_group.allow_ports["sg-rds"].id
}


output "pri_subnet_id" {
    value = aws_subnet.pri_subnet.id
}


output "pub_subnet_id" {
    value = aws_subnet.pub_subnet.id
}
