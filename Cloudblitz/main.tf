provider "aws" {
    region = "ap-southeast-2"
}

terraform {
    backend "s3" {
        region = "ap-southeast-2"
        key = "terraform.tfstate"
        bucket = "cbz-erp-tfstate"
    }
}

module "my_vpc" {
    source = "./modules/vpc"
    vpc_cidr = var.vpc_cidr
    region = var.region
    env = var.environment
    project = var.project
    pri_subnet_cidr = var.pri_subnet_cidr
    pub_subnet_cidr = var.pub_subnet_cidr
    pub_subnet_az = var.pub_subnet_az
    pri_subnet_az =  var.pri_subnet_az
    ports = var.sg_configuration
}

module "my_instance" {
    source = "./modules/ec2"
    #instances = var.instances
    instances = {
        jump_server = {image_id = var.jump_server_ami, instance_type = var.jump_instance_type, sg_id = [module.my_vpc.ssh_sg_id  ], key_name = var.ssh_key, subnet_id = module.my_vpc.pub_subnet_id},
        tomcat_server = {image_id = var.tomcat_server_ami, instance_type = var.tomcat_instance_type, sg_id = [module.my_vpc.ssh_sg_id, module.my_vpc.tomcat_sg_id ], key_name = var.ssh_key, subnet_id = module.my_vpc.pri_subnet_id},
        proxy_server = {image_id = var.proxy_server_ami, instance_type = var.proxy_instance_type, sg_id = [module.my_vpc.ssh_sg_id, module.my_vpc.http_sg_id], key_name = var.ssh_key, subnet_id = module.my_vpc.pub_subnet_id},
    }
    env = var.environment
}